#include "Game.h"

STATES state;

Game::Game(int numPlayers, PlayerData* playerDataArr)
{
	_numPlayers = numPlayers;
	for(int i=0; i<_numPlayers; i++)
	{
		_players.push_back(Player(playerDataArr[i], 0, i*30));
	}
}

Game::~Game()
{
}

void Game::loop()
{
	clock_t fpsTimer = clock();
	for(int i=0; i<_numPlayers; i++)
	{
		_players[i].initialize();
	}
	while(state != STATES(GAME_QUIT))
	{
		for(int i=0; i<_numPlayers; i++)
			_players[i].handleInput();
		while(fpsTimer + GLOBS::FPS >= clock()){}
		clear();
		for(int i=0; i<_numPlayers; i++)
		{
			_players[i].update();
			_players[i].render();
		}
		fpsTimer=clock();
	}
}
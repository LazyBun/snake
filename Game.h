#include "Player.h"



class Game
{
private:

int _numPlayers;
std::vector<Player> _players;

public:
	Game(int numPlayers, PlayerData* playerDataArr);
	~Game();
	void loop();
};
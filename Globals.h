#ifndef _GLOBALS_H
#define _GLOBALS_H
#include <cstdlib>

enum STATES 
{
	GAME_ON = 1,
	GAME_PAUSED = 2,
	GAME_MENU = 3,
	GAME_QUIT = 4,
	GAME_RESULT = 5,
	GAME_ON_MULTI = 6 
};

struct Pos
{
	int x;
	int y;
};

struct PlayerData
{
	char up, left, right, down, pause;
};


namespace GLOBS
{
	const int FPS = 200000;
	const int mapSize = 30;	
}

#endif

#include "Globals.h"
#include <vector>
#include <cstdio>
#include <queue>
#include <ncurses.h>
#include <time.h>
#include <random>


class Player
{
private:

char _map[GLOBS::mapSize][GLOBS::mapSize];
std::vector<Pos*> snake;
std::vector<Pos*> points;

std::queue<Pos> moves;

Pos gamePos;
PlayerData moveSet;

int count;
enum Direction
{
	UP,
	DOWN,
	LEFT,
	RIGHT
};	
Direction direction;

void addPoint();
Pos randomPos();

public:
	Player(PlayerData& data, int x, int y);
	~Player();

	void handleInput();
	void update();
	void render();

	void initialize();


};
#include "Player.h"


extern STATES state;



Player::Player(PlayerData& data, int x, int y)
{
	state = GAME_ON;
	count = 0;
	gamePos={x,y};
	moveSet.up = data.up;
	moveSet.down = data.down;
	moveSet.left = data.left;
	moveSet.right = data.right;
	moveSet.pause = data.pause;
}

Player::~Player()
{
	for(int i=0; i<this->snake.size(); i++)
	{
		delete this->snake[i];
	}
	for(int i=0; i<this->points.size(); i++)
	{
		delete this->points[i];
	}
}

Pos Player::randomPos()
{
	//<random> change
	static std::random_device rd;
	static std::uniform_int_distribution<int> uniform_dist(1, GLOBS::mapSize-2);
	Pos result{uniform_dist(rd), uniform_dist(rd)};
	for(int i=0; i<this->snake.size(); i++)
	{
		while(result.x == snake[i]->x && result.y == snake[i]->y)
		{
			result = {uniform_dist(rd), uniform_dist(rd)};
			break;
		}
	}
	return result;
}

void Player::addPoint()
{
	//add protection for duplicates
	Pos* pos = new Pos(randomPos());
	points.push_back(pos);
}

void Player::initialize()
{	//todo, return error?
	//setup map
	for(int i=0; i<GLOBS::mapSize; i++)
	{
		for(int j=0; j<GLOBS::mapSize; j++)
		{
			this->_map[i][j] = ' ';
		}		
	}
	//setup snake
	Pos* startSnake = new Pos();
	startSnake->x = GLOBS::mapSize/2;
	startSnake->y = GLOBS::mapSize/2;
	this->snake.push_back(startSnake);
	this->direction = UP;
	//setup point
	Pos* startPoint = new Pos(randomPos());
	this->points.push_back(startPoint);
}


void Player::handleInput()
{
	int ch;
	nodelay(stdscr, TRUE);
	ch = getch();

	if(ch == moveSet.up)
	{
		if(this->direction != Direction(DOWN))
			this->direction = Direction(UP);
	}
	if(ch == moveSet.down)
	{
		if(this->direction != Direction(UP))
			this->direction = Direction(DOWN);
	}
	if(ch == moveSet.left)
	{
		if(this->direction != Direction(RIGHT))
			this->direction = Direction(LEFT);
	}
	if(ch == moveSet.right)
	{
		if(this->direction != Direction(LEFT))
			this->direction = Direction(RIGHT);
	}
	if(ch == 'q')
	{
		state = STATES(GAME_QUIT);
	}
	if(ch == moveSet.pause)
	{
		//pause
		while(true)
		{
			int temp = getch();
			if(temp == moveSet.pause)
				break;
		}
	}

}

void Player::update()
{
	//add point if eaten last loop
	bool ate = false;
	if(points.size()==0)
	{
		addPoint();
		ate = true;
	}
	//move in direction
	std::queue<Pos> currentMoves;
	for(int i=0; i<this->snake.size(); i++)
	{
		Pos pos;
		if(i == 0)
		{
			switch(direction)
			{
				case Direction(UP):
				{
					snake[0]->x--;
					break;
				}
				case Direction(DOWN):
				{
					snake[0]->x++;
					break;
				}
				case Direction(LEFT):
				{
					snake[0]->y--;
					break;
				}
				case Direction(RIGHT):
				{
					snake[0]->y++;
					break;
				}
				default:
				break;
			}
			pos.x = snake[0]->x;
			pos.y = snake[0]->y;
			currentMoves.push(pos);
		}
		else
		{
			snake[i]->x = moves.front().x;
			snake[i]->y = moves.front().y;
			moves.pop();
			pos.x = snake[i]->x;
			pos.y = snake[i]->y;
			currentMoves.push(pos);
		}
	}
	//add new snake fragment if ate
	if(ate)
	{
		Pos* newFragment = new Pos();
		newFragment->x = moves.front().x;
		newFragment->y = moves.front().y;
		snake.push_back(newFragment);
	}
	moves.swap(currentMoves);
	//check if eat
	if(snake[0]->x == points[0]->x && snake[0]->y == points[0]->y)
	{
		delete points[0];
		points.pop_back();
	}
	//check if in wall
	if(snake[0]->x >=GLOBS::mapSize-1 || snake[0]->x <= 0 || snake[0]->y >=GLOBS::mapSize-1 || snake[0]->y <= 0)
	{
		//todo result screen, add to quit as well
		state = GAME_QUIT;
	}
	//check if head in tail
	for(int i=1; i<snake.size(); i++)
	{
		if(snake[0]->x == snake[i]->x && snake[0]->y == snake[i]->y)
		{
			state = GAME_QUIT;
			break;
		}
	}
}

void Player::render()
{
	move(gamePos.x,gamePos.y);
	//todo bettter score
	printw("Score: %d\n", snake.size());

	for(int i=0; i<GLOBS::mapSize; i++)
	{
		for(int j=0; j<GLOBS::mapSize; j++)
		{
			if(i==0 || j==0 || i==GLOBS::mapSize-1 || j==GLOBS::mapSize-1)
				this->_map[i][j] = 'w';
			else
				this->_map[i][j] = ' ';
		}		
	}
	for(int i=0; i<snake.size(); i++)
	{
		if(i==0)
			_map[snake[i]->x][snake[i]->y] = 'O';
		else
			_map[snake[i]->x][snake[i]->y] = 'o';
	}
	for(int i=0; i<points.size(); i++)
	{
		_map[points[i]->x][points[i]->y] = 'x';
	}
	for(int i=0; i<GLOBS::mapSize; i++)
	{
		for(int j=0; j<GLOBS::mapSize; j++)
		{
				printw("%c", _map[i][j]);
		}		
		move(i+2, gamePos.y);
	}
	printw("Steps: %d", count);
	count++;
}












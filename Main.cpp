#include "Game.h"

void showMenu();

extern STATES state;

int main()
{	
	initscr();
	noecho();
	state = GAME_MENU;

	while(state != GAME_QUIT)
	{
		//todo frames
		if(state == GAME_ON)
		{
			//play game
			PlayerData* data = new PlayerData();
			data->up = 'w';
			data->down = 's';
			data->left = 'a';
			data->right = 'd';
			data->pause = 'p';
			Game* game = new Game(1,data);
			game->loop();
			delete game;
			nodelay(stdscr, FALSE);
			state = GAME_MENU;
		}
		else if(state == GAME_ON_MULTI)
		{
			//play game
			PlayerData data[2];
			data[0].up = 'w';
			data[0].down = 's';
			data[0].left = 'a';
			data[0].right = 'd';
			data[0].pause = 'r';
			data[1].up = 'i';
			data[1].down = 'k';
			data[1].left = 'j';
			data[1].right = 'l';
			data[1].pause = 'p';
			Game* game = new Game(2,data);
			game->loop();
			delete game;
			nodelay(stdscr, FALSE);
			state = GAME_MENU;
		}
		else if(state == GAME_MENU)
		{
			showMenu();
		}
	}
	endwin();
}

void showMenu()
{
	char c;
	while(true)
	{
		printw("		Osom Snake\n");
		printw("		Controls:\n");
		printw("Q = POWERUP| W = UP | P = PAUSE\n");
		printw("A = LEFT| S = DOWN | D = RIGHT\n");
		printw("		| K = QUIT |\n");
		printw("Wat do?\n");
		printw("1. Play \n");
		printw("2. Play multi\n");
		printw("0. Quit\n");
		printw(": ");
		c = getch();
		if(c == '1')
		{
			state = GAME_ON;
			break;
		}
		if(c == '2')
		{
			state = GAME_ON_MULTI;
			break;
		}
		if(c == '0')
		{
			state = GAME_QUIT;
			break;
		}
		clear();
	}
}